// Import the Chart constructor from chart.js/auto found in node_modules with the Import by default
import Chart from "chart.js/auto"

// Indicate the context of the chart by giving an id to the canvas created in HTML
const ctx = document.getElementById("new-chart").getContext("2d");

// Create two empty arrays that will be populated by the data received through the socket
const data = [];
const labels = [];

// Create the chart 
const chart = new Chart(ctx, {
    type: 'line',
    data: {
        labels,
        datasets: [{
            label: 'Esercizio 20-12-2021',
            data,
            fill: false,
            borderColor: "rgb(75, 192, 192)",
            tension: 0.1
        }],
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});

// Socket
const sck = new WebSocket("ws://localhost:9000");

// Add a listener to the message event
sck.addEventListener("message", ev => {
    // Create a constant that contains the values given by the server and convert them in an array of strings with JSON.parse
    const xy = JSON.parse(ev.data);

    // Put the values "value" and "time" in the constants data and labels that correspond to to the axis of the X and of the Y
    // Have to convert the value "value" from string to number
    data.push(+xy.value);
    labels.push(xy.time);
    // Update the chart
    chart.update();
})